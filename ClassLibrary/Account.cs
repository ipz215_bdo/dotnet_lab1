﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ClassLibrary
{
    
    public class Account
    {
        public string cardNumber;
        public string Name;
        public int Balance = 5000;

        public Account(string _cardNumber,string name)
        {
            cardNumber = _cardNumber;
            Name = name;
        }
        public Account() { }
    }
}
