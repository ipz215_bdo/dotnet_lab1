﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClassLibrary
{
    public class AutomatedTellerMachine
    {
        public int amountOfMoneyInATM = 50000;    // Кількість грошей в банкоматі
        public int idATM = 212;                 // Id банкомату
        public string adressATM = "м.Житомир вул.Хлібна 39"; // Адреса банкомату

        public void Print_information_in_console()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine($"\nID банкомату: {idATM}\nАдреса банкомату: {adressATM}\nКількість грошей у банкоматі: {amountOfMoneyInATM} гривень");
            Console.ReadLine();
        }
     
    }
}
