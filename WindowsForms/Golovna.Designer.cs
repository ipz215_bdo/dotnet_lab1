﻿namespace WindowsForms
{
    partial class Golovna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.But_Balanse = new System.Windows.Forms.Button();
            this.But_Kosht = new System.Windows.Forms.Button();
            this.But_Cart = new System.Windows.Forms.Button();
            this.But_perecaz = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.TextMessage = new System.Windows.Forms.TextBox();
            this.Login = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textNum = new System.Windows.Forms.TextBox();
            this.textPassword = new System.Windows.Forms.TextBox();
            this.textName = new System.Windows.Forms.TextBox();
            this.Clear = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // But_Balanse
            // 
            this.But_Balanse.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.But_Balanse.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.But_Balanse.Location = new System.Drawing.Point(12, 456);
            this.But_Balanse.Name = "But_Balanse";
            this.But_Balanse.Size = new System.Drawing.Size(194, 74);
            this.But_Balanse.TabIndex = 2;
            this.But_Balanse.Text = "Перевірити баланс на картці";
            this.But_Balanse.UseVisualStyleBackColor = true;
            this.But_Balanse.Visible = false;
            this.But_Balanse.Click += new System.EventHandler(this.But_Balanse_Click);
            // 
            // But_Kosht
            // 
            this.But_Kosht.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.But_Kosht.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.But_Kosht.Location = new System.Drawing.Point(225, 456);
            this.But_Kosht.Name = "But_Kosht";
            this.But_Kosht.Size = new System.Drawing.Size(250, 40);
            this.But_Kosht.TabIndex = 2;
            this.But_Kosht.Text = "Зняти кошти";
            this.But_Kosht.UseVisualStyleBackColor = true;
            this.But_Kosht.Visible = false;
            this.But_Kosht.Click += new System.EventHandler(this.But_Kosht_Click);
            // 
            // But_Cart
            // 
            this.But_Cart.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.But_Cart.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.But_Cart.Location = new System.Drawing.Point(498, 456);
            this.But_Cart.Name = "But_Cart";
            this.But_Cart.Size = new System.Drawing.Size(205, 42);
            this.But_Cart.TabIndex = 2;
            this.But_Cart.Text = "Поповнити картку";
            this.But_Cart.UseVisualStyleBackColor = true;
            this.But_Cart.Visible = false;
            this.But_Cart.Click += new System.EventHandler(this.But_Cart_Click);
            // 
            // But_perecaz
            // 
            this.But_perecaz.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.But_perecaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.But_perecaz.Location = new System.Drawing.Point(722, 456);
            this.But_perecaz.Name = "But_perecaz";
            this.But_perecaz.Size = new System.Drawing.Size(268, 42);
            this.But_perecaz.TabIndex = 2;
            this.But_perecaz.Text = "Зробити переказ";
            this.But_perecaz.UseVisualStyleBackColor = true;
            this.But_perecaz.Visible = false;
            this.But_perecaz.Click += new System.EventHandler(this.But_perecaz_Click);
            // 
            // Exit
            // 
            this.Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.74545F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Exit.Location = new System.Drawing.Point(923, 12);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(88, 44);
            this.Exit.TabIndex = 4;
            this.Exit.Text = "Exit";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // TextMessage
            // 
            this.TextMessage.AllowDrop = true;
            this.TextMessage.Location = new System.Drawing.Point(814, 62);
            this.TextMessage.Multiline = true;
            this.TextMessage.Name = "TextMessage";
            this.TextMessage.Size = new System.Drawing.Size(197, 319);
            this.TextMessage.TabIndex = 5;
            // 
            // Login
            // 
            this.Login.BackColor = System.Drawing.Color.GreenYellow;
            this.Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.70909F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Login.Location = new System.Drawing.Point(354, 284);
            this.Login.Name = "Login";
            this.Login.Size = new System.Drawing.Size(300, 56);
            this.Login.TabIndex = 12;
            this.Login.Text = "Увійти";
            this.Login.UseVisualStyleBackColor = false;
            this.Login.Click += new System.EventHandler(this.Login_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.78182F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.Green;
            this.label3.Location = new System.Drawing.Point(433, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "НОМЕР КАРТКИ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.78182F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.Green;
            this.label4.Location = new System.Drawing.Point(459, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "ПАРОЛЬ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.78182F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.Green;
            this.label5.Location = new System.Drawing.Point(394, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(225, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "ВАШЕ ПРІЗВИЩЕ ТА ІМ\'Я";
            // 
            // textNum
            // 
            this.textNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.74545F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textNum.Location = new System.Drawing.Point(356, 114);
            this.textNum.Name = "textNum";
            this.textNum.Size = new System.Drawing.Size(300, 28);
            this.textNum.TabIndex = 6;
            this.textNum.Click += new System.EventHandler(this.textNum_Click);
            // 
            // textPassword
            // 
            this.textPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.74545F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textPassword.Location = new System.Drawing.Point(354, 177);
            this.textPassword.Name = "textPassword";
            this.textPassword.Size = new System.Drawing.Size(300, 28);
            this.textPassword.TabIndex = 7;
            this.textPassword.Click += new System.EventHandler(this.textPassword_Click);
            this.textPassword.TextChanged += new System.EventHandler(this.textPassword_TextChanged);
            // 
            // textName
            // 
            this.textName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.74545F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textName.Location = new System.Drawing.Point(356, 238);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(300, 28);
            this.textName.TabIndex = 8;
            this.textName.Click += new System.EventHandler(this.textName_Click);
            // 
            // Clear
            // 
            this.Clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.74545F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Clear.Location = new System.Drawing.Point(814, 12);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(92, 44);
            this.Clear.TabIndex = 4;
            this.Clear.Text = "Clear";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.74545F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(225, 502);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(250, 28);
            this.textBox1.TabIndex = 7;
            this.textBox1.Visible = false;
            this.textBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox1_MouseClick);
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.74545F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox2.Location = new System.Drawing.Point(498, 504);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(205, 28);
            this.textBox2.TabIndex = 7;
            this.textBox2.Visible = false;
            this.textBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox2_MouseClick);
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.74545F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox3.Location = new System.Drawing.Point(722, 504);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(268, 28);
            this.textBox3.TabIndex = 7;
            this.textBox3.Visible = false;
            this.textBox3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox3_MouseClick);
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.74545F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox4.Location = new System.Drawing.Point(722, 541);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(268, 28);
            this.textBox4.TabIndex = 7;
            this.textBox4.Visible = false;
            this.textBox4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox4_MouseClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::WindowsForms.Properties.Resources.image_removebg_preview;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(-41, -101);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(353, 195);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // Golovna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Purple;
            this.ClientSize = new System.Drawing.Size(1023, 593);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Login);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textNum);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textPassword);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.TextMessage);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.But_perecaz);
            this.Controls.Add(this.But_Cart);
            this.Controls.Add(this.But_Kosht);
            this.Controls.Add(this.But_Balanse);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Golovna";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Golovna_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button But_Balanse;
        private System.Windows.Forms.Button But_Kosht;
        private System.Windows.Forms.Button But_Cart;
        private System.Windows.Forms.Button But_perecaz;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.TextBox TextMessage;
        private System.Windows.Forms.Button Login;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox textNum;
        public System.Windows.Forms.TextBox textPassword;
        public System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Button Clear;
        public System.Windows.Forms.TextBox textBox1;
        public System.Windows.Forms.TextBox textBox2;
        public System.Windows.Forms.TextBox textBox3;
        public System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

