﻿using ClassLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace WindowsForms
{
    public delegate void NoParams();
    public delegate bool Veref(string pass, string cartnum, string name);
  
    public partial class Golovna : Form
    {
        Account _account = new Account();
        Registered _registered = new Registered();

        public static event NoParams _noParams;
        public static event Veref _verefication;
       
        public Golovna()
        {
            InitializeComponent();
        }

        private void Golovna_Load(object sender, EventArgs e)
        {
            textNum.Text = "XXXX XXXX XXXX XXXX";
            textPassword.Text = "****";
            textName.Text = "Іванов Іван";
            textBox1.Text = "Введіть суму для зняття";
            textBox2.Text = "Введіть суму для поповнення";
            textBox2.Text = "Введіть суму для поповнення";
            textBox3.Text = "Введіть суму для переказу";
            textBox4.Text = "Введіть номер картки для переказу";
        }

        public void But_Balanse_Click(object sender, EventArgs e)
        {
            TextMessage.Text = $"Номер карти: {_account.cardNumber}\nВласник карти: {_account.Name}\nБаланс на картці: {_account.Balance}";
        }

        public void Login_Click(object sender, EventArgs e)
        {
            _verefication += _registered.VerefiRegistred;

            if (_verefication(textPassword.Text, textNum.Text, textName.Text) == true)
            {
                Golovna golovna = new Golovna();
                MessageBox.Show("Вхід виконано успішно!");
                _account.cardNumber = textNum.Text;
                _account.Name = textName.Text;
                _noParams += Messages;
                _noParams();
                TextMessage.Text = $"Номер карти: {_account.cardNumber}\n" +
                                    $"Власник карти: {_account.Name}\n" +
                                    $"Баланс на картці: {_account.Balance}\n";
                Login.Enabled = false;

                But_Balanse.Visible = true;
                But_Kosht.Visible = true;
                But_Cart.Visible = true;
                But_perecaz.Visible = true;
                textBox1.Visible = true;
                textBox2.Visible = true;
                textBox3.Visible = true;
                textBox4.Visible = true;
            }
            else
            {
                MessageBox.Show("Дані введено невірно");
            }
            
        }
        public static void Messages()
        {
            AutomatedTellerMachine machine = new AutomatedTellerMachine();
            MessageBox.Show($"\nID банкомату: {machine.idATM}.\n{machine.adressATM}\nКількість грошей у банкоматі: {machine.amountOfMoneyInATM} гривень.");
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            TextMessage.Clear();
        }

        private void But_Kosht_Click(object sender, EventArgs e)
        {
         
            _verefication += _registered.VerefiRegistred;
            if (_verefication(textPassword.Text, textNum.Text, textName.Text) == true)
            {
               
                if (textBox1.Text != "Введіть суму для зняття")
                {
                    int sum = Convert.ToInt32(textBox1.Text);

                    if (sum <= _account.Balance)
                    {
                        _account.Balance -= sum;
                        TextMessage.Text = $"Номер карти:{_account.cardNumber}\nВласник карти:{_account.Name}\nБаланс на картці:{_account.Balance}\n";
                        textBox1.Text = "";
                    }
                    else
                    {
                        MessageBox.Show("Недостатньо коштів на картці(");
                    }
                }
                else
                {
                    MessageBox.Show("Введіть суму зняття");

                }
            }
            else
            {
                MessageBox.Show("Введіть коректно дані");
            }
            textBox1.Text = "Введіть суму для зняття";
            textBox1.BackColor = Color.Silver;
        }

        private void textPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void But_Cart_Click(object sender, EventArgs e)
        {
           
            _verefication += _registered.VerefiRegistred;
            if (_verefication(textPassword.Text, textNum.Text, textName.Text) == true)
            {

              
                if (textBox2.Text  != "Введіть суму для поповнення")
                {
                    int sum = Convert.ToInt32(textBox2.Text);
                    _account.Balance += sum;
                    TextMessage.Text = $"Номер карти: {_account.cardNumber}\n" +
                        $"Власник карти: {_account.Name}\n" +
                        $"Баланс на картці: {_account.Balance}\n";
                    textBox2.Text = "";
                }
                else
                {
                    MessageBox.Show("Введіть суму поповнення");

                }
            }
            else
            {
                MessageBox.Show("Введіть коректно дані");
            }
            textBox2.Text = "Введіть суму для поповнення";
            textBox2.BackColor = Color.Silver;

        }

        private void But_perecaz_Click(object sender, EventArgs e)
        {
            _verefication += _registered.VerefiRegistred;
            if (_verefication(textPassword.Text, textNum.Text, textName.Text) == true)
            {
                if (textBox4.Text != "Введіть номер картки для переказу") {
                    int sum = Convert.ToInt32(textBox3.Text);
                    DialogResult chek = MessageBox.Show("Відправити чек на email", "Чек", MessageBoxButtons.YesNo);
                    if (sum != 0) {   
                        if (sum <= _account.Balance)
                        {
                            _account.Balance -= sum;
                            TextMessage.Text = $"Номер карти: {_account.cardNumber}\n" +
                                $"Власник карти: {_account.Name}\n" +
                                $"Баланс на картці: {_account.Balance}\n";
                            if (chek == DialogResult.Yes)
                            {
                                var smtpClient = new SmtpClient("smtp.gmail.com")
                                {
                                    Port = 587,
                                    UseDefaultCredentials = false,
                                    Credentials = new NetworkCredential("ipz215_bdo@student.ztu.edu.ua", "428237"),
                                    EnableSsl = true,
                                    DeliveryMethod = SmtpDeliveryMethod.Network
                                };
                                smtpClient.Send("ATM-PrivateBank@gmail.com", "ipz215_bdo@student.ztu.edu.ua", "Банкомат №018,квитанція переказу", $"З вашої картки було здійснено переказ на суму {sum} гривень");
                                MessageBox.Show("Операції здійснено успішно , чек відправлено вам на email");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Недостатньо коштів на рахунку(");
                        }
                    }
                   
                }
                else
                {
                    MessageBox.Show("Введіть коректно дані");
                }
            }
            textBox4.Text = "Введіть номер картки для переказу";
            textBox3.Text = "Введіть суму для переказу";
        }

        private void textBox1_MouseClick(object sender, MouseEventArgs e)
        {
            textBox1.Text = "";
            textBox1.BackColor = default;
            textBox2.Text = "Введіть суму для поповнення";
            textBox3.Text = "Введіть суму для переказу";
            textBox4.Text = "Введіть номер картки для переказу";

        }

        private void textBox2_MouseClick(object sender, MouseEventArgs e)
        {
            textBox2.Text = "";
            textBox2.BackColor = default;
            textBox1.Text = "Введіть суму для зняття";
            textBox3.Text = "Введіть суму для переказу";
            textBox4.Text = "Введіть номер картки для переказу";
        }

        private void textBox3_MouseClick(object sender, MouseEventArgs e)
        {
            textBox3.Text = "";
            textBox3.BackColor = default;
            textBox1.Text = "Введіть суму для зняття";
            textBox2.Text = "Введіть суму для поповнення";
           
        }

        private void textBox4_MouseClick(object sender, MouseEventArgs e)
        {
            textBox4.Text = "";
            textBox4.BackColor = default;
            textBox1.Text = "Введіть суму для зняття";
            textBox2.Text = "Введіть суму для поповнення";
           
        }
        private void textNum_Click(object sender, EventArgs e)
        {
            textNum.Text = "";
            textNum.BackColor = default;
        }

        private void textPassword_Click(object sender, EventArgs e)
        {
            textPassword.Text = "";
            textPassword.BackColor = default;
        }

        private void textName_Click(object sender, EventArgs e)
        {
            textName.Text = "";
            textName.BackColor = default;
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

    }
}
