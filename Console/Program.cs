﻿using System;
using System.Net.Mail;
using System.Net;
using ClassLibrary;
using System.Text;

namespace ConsoleApp
{
    public delegate void NoParam();
    public delegate bool Verification(string pass, string cartnum, string name);
    public class Program
    {
        
        public static event NoParam _noParam;
        public static event Verification _verification;
        
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            
            Registered _registered = new Registered();
            _verification += _registered.VerefiRegistred;
            int i = 0;
            Account _account = new Account();
            while (i < 3)
            {
                Console.WriteLine("Введіть номер карти:");
                string cardNum = Console.ReadLine();
                Console.WriteLine("Введіть пароль:");
                string password = Console.ReadLine();
                Console.WriteLine("Введіть прізвище та ім'я:");
                string name = Console.ReadLine();
                _account.cardNumber = cardNum;
                _account.Name = name;

                if (_verification(password, cardNum,name) == true)
                {


                    Console.WriteLine("\nВхід виконано успішно!");
                    _noParam += Message;
                    _noParam();
                    break;
                }
                else
                {
                    Console.WriteLine("Невірні дані!");
                    i++;
                }
            }
           
            while (true)
            {
               
                Console.WriteLine("Оберіть дію:\n0 - Завершити роботу\n1 - Перевірити баланс на картці" +
                    "\n2 - Поповнити картку\n3 - Зняти кошти\n4 - Зробити переказ\n");

                int n = Convert.ToInt32(Console.ReadLine());
                
                if(n == 0)
                {
                    Environment.Exit(0);
                }
                if (n == 1)
                {
                    Console.WriteLine($"Номер карти: {_account.cardNumber}\nВласник карти: {_account.Name}\nБаланс на картці: {_account.Balance}");
                }
                if (n == 2)
                {
                    Console.WriteLine("\nВкажіть суму для поповнення картки: ");
                    int sum = Convert.ToInt32(Console.ReadLine());
                    _account.Balance += sum;
                    Console.WriteLine($"Залишок:{_account.Balance} гривень.");
                    
                }
                if (n == 3)
                {
                    Console.WriteLine("\nВкажіть суму для зняття з картки: ");
                    int sum = Convert.ToInt32(Console.ReadLine());
                    if (sum <= _account.Balance)
                    {
                        _account.Balance -= sum;
                        Console.WriteLine($"Залишок: {_account.Balance} гривень.");
                    }
                    else
                    {
                        Console.WriteLine("Недостатньо коштів на рахунку:(");
                    } 
                }
                if (n == 4)
                {
                    Console.WriteLine("\nВкажіть суму для переказу з картки: ");
                    int sum = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("\nВкажіть номер картки для переказу: ");
                    string cart = Console.ReadLine();
                    Console.WriteLine("\nНадіслати чек на email? \n1 - так \n2 - ні");
                    int chek = Convert.ToInt32(Console.ReadLine());
                    
                    if (sum <= _account.Balance)
                    {
                        _account.Balance -= sum;
                        Console.WriteLine($"Залишок: {_account.Balance} гривень.");
                        if (chek == 1)
                        {
                            var smtpClient = new SmtpClient("smtp.gmail.com")
                            {
                                Port = 587,
                                UseDefaultCredentials = false,
                                Credentials = new NetworkCredential("ipz215_bdo@student.ztu.edu.ua", "428237"),
                                EnableSsl = true,
                                DeliveryMethod = SmtpDeliveryMethod.Network
                            };
                            smtpClient.Send("ATM-PrivateBank@gmail.com", "ipz215_bdo@student.ztu.edu.ua", "Банкомат №018,квитанція переказу", $"З вашої картки було здійснено переказ на суму {sum} гривень");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Недостатньо коштів на рахунку!");
                    }
                }
            }
        }
        public static void Message()
        {
            AutomatedTellerMachine machine = new AutomatedTellerMachine();
            machine.Print_information_in_console();
        } 
    }
}
